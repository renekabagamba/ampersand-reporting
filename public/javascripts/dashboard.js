$(document).ready(function () {

  // Key STATS
  $.ajax({
      url: "/api/key-stats",
      dataType: "json",
      type: "GET",
      success: function (data, xhr) {

          const ToDate = new Date();
          ToDate.setHours(0,0,0,0);

          const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
          var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

          $.each(data.keyStats, function (key, value) {

              if( new Date(this._id).getTime() < ToDate.getTime() ) {

                let statsDate = new Date(this._id)

                $("#keyStats").append(
                  '<h6 class="font-weight text-left">' + days[statsDate.getDay()] + ', '
                                                       + statsDate.getDate() + ' '
                                                       + months[statsDate.getMonth()] + ' '
                                                       + statsDate.getFullYear()
                                                       + '</h6>'
                );

                $("#keyStatsAvgSwaps").append(
                  '<p class="card-text blue-text">' +
                        '<i class="fas fa-thumbs-up fa-2x"></i>' +
                        '<span class="ml-2" style="font-size: 30px;">' + this.avgDailySwaps.toFixed() +
                        '</span>,' + ((this.avgDailySwaps % 1) * 100).toFixed() +
                  '</p>'
                );

                $("#keyStatsAvgRide").append(
                  '<p class="card-text red-text">' +
                    '<i class="fas fa-thumbs-down fa-2x"></i></i>' +
                    '<span class="ml-2" style="font-size: 30px;">' + this.avgDailyDistanceCovered.toFixed() +
                    '</span>,' + ((this.avgDailyDistanceCovered % 1) * 100).toFixed() + ' Km' +
                  '</p>'
                );

                $("#keyStatsAvgEnergy").append(
                  '<p class="card-text green-text">' +
                  '<i class="fas fa-angle-double-up fa-2x"></i>' +
                  '<span class="ml-2" style="font-size: 30px;">' + this.avgDailyEnergyUsed.toFixed() +
                  '</span>,' + ((this.avgDailyEnergyUsed % 1) * 100).toFixed() + ' Ah' +
                  '</p>'
                );

                return false; // breaks from loop

              }

          });
      },
      error: function (data, xhr) {

      },
  });


  // SWAP STATS table
  $.ajax({
      url: "/api/swap-stats",
      dataType: "json",
      type: "GET",
      success: function (data, xhr) {
          $.each(data.swapStats, function (key, value) {
              if (this.energyUsed) {
                $("#swapStats").append(
                  '<tr>' +
                    '<th scope="row">'+ (key+1) +'</th>' +
                    '<td>' + this.swapTransactionId.driver.firstName + " "
                    + this.swapTransactionId.driver.lastName +'</td>' +
                    '<td>'+ this.swapTransactionId.chargingStation.location +'</td>' +
                    '<td>'+ this.energyUsed.toFixed(2) +'</td>' +
                    '<td>'+ this.distance.toFixed(2) +'</td>' +
                    '<td>'+ this.createdAt +'</td>' +
                  '</tr>'
                );
              } else {
                $("#swapStats").append(
                  '<tr>' +
                    '<th scope="row">'+ (key+1) +'</th>' +
                    '<td>' + this.swapTransactionId.driver.firstName + " "
                    + this.swapTransactionId.driver.lastName +'</td>' +
                    '<td>'+ this.swapTransactionId.chargingStation.location +'</td>' +
                    '<td> Not Available </td>' +
                    '<td> Not Available </td>' +
                    '<td>'+ this.createdAt +'</td>' +
                  '</tr>'
                );
              }
          });
      },
      error: function (data, xhr) {

      },
  });

  // Daily Swaps table
  $.ajax({
      url: "/api/daily-swaps",
      dataType: "json",
      type: "GET",
      success: function (data, xhr) {

          $.each(data.dailySwapStats, function (key, value) {

                $("#dailyDriverSwaps").append(
                  '<tr>' +
                    '<th scope="row">'+ (key+1) +'</th>' +
                    '<td>' + this.driver[0].firstName + " " + this.driver[0].lastName + '</td>' +
                    '<td>'+ this.dailySwapCount +'</td>' +
                    '<td>'+ this.dailyEnergyUsed.toFixed(2) +'</td>' +
                    '<td>'+ this.dailyDistanceCovered.toFixed(2) +'</td>' +
                    '<td>' + this._id.date + '</td>' +
                  '</tr>'
                );
          });
      },
      error: function (data, xhr) {

      },
  });


});
