$(document).ready(function () {

    // ----------------------------------------------------------
    // SelectBox for Charging Stations
    // ----------------------------------------------------------
    $("#charging_station_select_box").selectpicker({
        liveSearch: true,
        size:5,
        title:'Select Charging Station',
        dropupAuto: false
    });

    $.ajax({
        url: "/api/charging-stations",
        dataType: "json",
        type: "GET",
        success: function (data, xhr) {
            $.each(data.chargingStations, function (key, value) {
                $("select[name='chargingStation']").append("<option value='"+ this._id +"'>" + this.location + "</option>");
            });
            $("#charging_station_select_box").selectpicker("refresh");
        },

        error: function (data, xhr) {

        },
    });

    // ----------------------------------------------------------
    // SelectBox for Drivers
    // ----------------------------------------------------------
    $("#driver_select_box").selectpicker({
        liveSearch: true,
        size:5,
        title:'Select Driver',
        dropupAuto: false
    });

    $.ajax({
        url: "/api/drivers",
        dataType: "json",
        type: "GET",
        success: function (data, xhr) {
            $.each(data.drivers, function (key, value) {
                $("select[name='driver']").append("<option value='" + this._id +"'>"
                + this.firstName + " " + this.lastName + " | " + this.vehicle.plateNo + "</option>");
            });
            $("#driver_select_box").selectpicker("refresh");
        },

        error: function (data, xhr) {

        },
    });

      // ----------------------------------------------------------
      // Text field Battery IN
      // ----------------------------------------------------------
      $("select[name='driver']").on('change', function() {

        // Reset previous driver data
        $("input[name='fee']").val('');
        $("input[name='prevBatteryOutCharge']").val('');
        $("input[name='prevOdometerReading']").val('');

        $("input[name='batteryIn']").prop('disabled', false);
        $("input[name='prevBatteryOutCharge']").prop('disabled', false);
        $("input[name='prevOdometerReading']").prop('disabled', false);

        $("select[name='batteryInCharge']").prop('disabled', false);
        $("select[name='batteryInCharge']").prop('required', true);

          $.ajax({
              url: "/api/swap-transactions/" + this.value,
              dataType: "json",
              type: "GET",
              success: function (data, xhr) {
                  $("input[name='fee']").val(500);

                  if(data.lastSwap){
                    $("input[name='batteryInSn']").val(data.lastSwap.batteryOut.serialNo);
                    $("input[name='batteryIn']").val(data.lastSwap.batteryOut._id);

                    $("input[name='prevBatteryOutCharge']").val(data.lastSwap.batteryOutCharge);
                    $("input[name='prevOdometerReading']").val(data.lastSwap.odometerReading);

                  } else {
                    $("input[name='batteryInSn']").val("No Battery");

                    $("input[name='batteryIn']").prop('disabled', true);
                    $("input[name='prevBatteryOutCharge']").prop('disabled', true);
                    $("input[name='prevOdometerReading']").prop('disabled', true);

                    $("select[name='batteryInCharge']").prop('disabled', true);
                    $("select[name='batteryInCharge']").prop('required', false);
                  }

              },

              error: function (data, xhr) {
              },
          });

      });

    // ----------------------------------------------------------
    // SelectBox for Battery IN charge
    // ----------------------------------------------------------
    $("#battery_in_charge_select_box").selectpicker({
        liveSearch: true,
        size:5,
        title:'Select Battery Charge',
        dropupAuto: false
    });

    let percentage;
    for (percentage = 0; percentage <= 100; percentage++) {
      $("select[name='batteryInCharge']").append("<option value='" + percentage + "'>" + percentage + "%" + "</option>");
    }
    $("#battery_in_charge_select_box").selectpicker("refresh");

    // ----------------------------------------------------------
    // SelectBox for Battery OUT
    // ----------------------------------------------------------
    $("#battery_out_select_box").selectpicker({
        liveSearch: true,
        size:5,
        title:'Select Battery',
        dropupAuto: false
    });

    $.ajax({
        url: "/api/available-batteries",
        dataType: "json",
        type: "GET",
        success: function (data, xhr) {
            $.each(data.availableBatteries, function (key, value) {
                $("select[name='batteryOut']").append("<option value='"
                + this._id + "'>" + this.serialNo + "</option>");
            });
            $("#battery_out_select_box").selectpicker("refresh");
        },

        error: function (data, xhr) {

        },
    });

    // ----------------------------------------------------------
    // SelectBox for Battery OUT charge
    // ----------------------------------------------------------
    $("#battery_out_charge_select_box").selectpicker({
        liveSearch: true,
        size:5,
        title:'Select Battery Charge',
        dropupAuto: false
    });

    for (percentage = 100; percentage >= 0; percentage--) {
      $("select[name='batteryOutCharge']").append("<option value='" + percentage + "'>" + percentage + "%" + "</option>");
    }
    $("#battery_out_charge_select_box").selectpicker("refresh");

    // ----------------------------------------------------------
    // SelectBox for Battery OUT charge
    // ----------------------------------------------------------
    $('#swap-transaction-form-div').on('submit', function(e) {

        e.preventDefault();
        var formData = $("form#swapTransactionForm").serialize();

        // process the form
        $.ajax({
                type: 'POST',
                url: '/api/swap-transactions',
                data: formData,
                dataType: 'json',
                encode: true
            })
            .done(function(data) {
              // Success messsage
              $("#messsage").html(
                "<div class='alert alert-success alert-dismissible fade show' role='alert'>"
                + "Swap Transaction Successfully Submitted!</div>");

              // Refresh form after a second
              setTimeout(function (){
                location.reload();
                }, 1000);

            })
            .fail(function(data) {

              $("#messsage").html(
                "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"
                + "Failed to Save Transaction! Contact System Administrator!</div>");

            });
    });

});
