import ChargingStation from '../models/chargingStation.model';
import Driver from '../models/driver.model';
import Battery from '../models/battery.model';
import Vehicle from '../models/vehicle.model';
import SwapTransaction from '../models/swapTransaction.model';
import SwapStats from '../models/swapStats.model';

import cuid from 'cuid';

const SwapBatteryController = {};

SwapBatteryController.getAllChargingStations = (req, res) => {
    try{
        ChargingStation.find().exec((err, chargingStations) => {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.status(200).json({ chargingStations });
            return;
        });
    }
    catch(err){
        res.send(err);
        return;
    }
};

SwapBatteryController.getAllDrivers = (req, res) => {
    try{
        Driver.find()
              .populate({ path: 'vehicle', model: Vehicle })
              .exec((err, drivers) => {
            if (err) {
                console.log(err);
                res.status(500).send(err);
                return;
            }
            res.status(200).json({ drivers });
            return;
        });
    }
    catch(err){
        console.log(err);
        res.send(err);
        return;
    }
};

SwapBatteryController.getAllAvailableBatteries = (req, res) => {
  try{
      Battery.find().exec((err, batteries) => {
          if (err) {
              res.status(500).send(err);
              return;
          }

          let availableBatteries = [];

          for (let i = 0; i < batteries.length; i++) {
            if (!( global.allBatteriesOut.has( String(batteries[i]._id) ))){
              availableBatteries.push(batteries[i]);
            }
          }

          res.status(200).json({ availableBatteries });
          return;
      });
  }
  catch(err){
      res.send(err);
      return;
  }
};

SwapBatteryController.getLastSwap = (req, res) => {
  try {
    SwapTransaction.findOne({'driver': req.params.driverId })
                   .sort('-createdAt')
                   .populate({ path: 'batteryOut', model: Battery })
                   .exec((err, lastSwap) => {
                      if (err) {
                          res.status(500).send(err);
                          return;
                      }

                      res.status(200).json({ lastSwap });
                      return;
                  });
  }
  catch(err){
      res.send(err);
      return;
  }
};


SwapBatteryController.recordSwapTransaction = (req, res) => {
    try {
        if (  !req.body.batteryOut
           || !req.body.fee
           || !req.body.chargingStation
           || !req.body.driver
           || !req.body.odometerReading
           || !req.body.batteryOutCharge ){
            res.status(400).json({message :
                            'Swap Transaction information incomplete!' });
        }

        const newSwap = new SwapTransaction(req.body);

        newSwap.cuid = cuid();

        newSwap.save(async (err, saved) => {
            if (err) {
                res.status(500).send(err);
                return;
            }

            console.log(typeof req.body.batteryOut)
            // Save Battery out
            global.allBatteriesOut.add(req.body.batteryOut);


            console.log("Current All Batteries Out:");
            console.log(global.allBatteriesOut);

            // Compute and record swap stats
            let newSwapStats;
            if (req.body.batteryIn){

              // Remove Prev battery from list of batteries out
              global.allBatteriesOut.delete(req.body.batteryIn);

              const inBattery = await Battery.findById(
                                          req.body.batteryIn);

              let energyUsed = (req.body.prevBatteryOutCharge
                              - req.body.batteryInCharge)/100
                              * (inBattery.voltage * inBattery.capacity);
              let distanceTravelled = req.body.odometerReading
                                - req.body.prevOdometerReading;

              newSwapStats = new SwapStats({
                              "swapTransactionId" : saved._id,
                              "energyUsed" : energyUsed,
                              "distance" : distanceTravelled
                            });

            } else {
              newSwapStats = new SwapStats({
                              "swapTransactionId" : saved._id
                            });
            }

            console.log("Current All Batteries Out - after swap:");
            console.log(global.allBatteriesOut);

            newSwapStats.cuid = cuid();

            newSwapStats.save((err, stats) => {
                  if (err) {
                    res.status(500).send(err);
                    return;
                  }
                  res.status(200).json({ swapTransaction: saved });
                  return;
              });

        });
    }
    catch (err) {
        res.send(err);
        return;
    }
};

export default SwapBatteryController;
