import Vehicle from '../models/vehicle.model';
import Driver from '../models/driver.model';
import Battery from '../models/battery.model';
import ChargingStation from '../models/chargingStation.model';
import User from '../models/user.model';
import SwapTransaction from '../models/swapTransaction.model';
import SwapStats from '../models/swapStats.model';

import cuid from 'cuid';

const TestDataController = {};

TestDataController.getAllVehicles = (req, res) => {
    try{
        Vehicle.find().sort('').exec((err, vehicles) => {
            if (err) {
                res.status(500).send(err);
            }
            res.status(200).json({ vehicles });
        });
    }
    catch(err){
        res.send(err);
    }
};

//Create A Vehicle
TestDataController.createVehicle = (req, res) => {
    try {
        if (!req.body.vehicle.brand || !req.body.vehicle.model ||
            !req.body.vehicle.plateNo) {
            res.status(400).json({message:'Vehicle information incomplete!'});
        }

        const newVehicle = new Vehicle(req.body.vehicle);

        newVehicle.cuid = cuid();

        newVehicle.save((err, saved) => {
            if (err) {
                res.status(500).send(err);
            }
            res.status(200).json({ vehicle: saved });
        });
    }
    catch (err) {
        console.log(err);
    }
}

//Create A Driver
TestDataController.createDriver = (req, res) => {
    try {
        if (!req.body.driver.firstName || !req.body.driver.lastName ||
             !req.body.driver.phoneNumber || !req.body.driver.vehicle ){
            res.status(400).json({message:'Driver information incomplete!'});
        }

        const newDriver = new Driver(req.body.driver);

        newDriver.cuid = cuid();

        newDriver.save((err, saved) => {
            if (err) {
                res.status(500).send(err);
            }
            res.status(200).json({ driver: saved });
        });
    }
    catch (err) {
        console.log(err);
    }
}

//TODO: Assign Vehicle to Driver
// TestDataController.assignVehicleToDriver = (req, res) => {
// }

//Create A Battery
TestDataController.createBattery = (req, res) => {
    try {
        if (!req.body.battery.serialNo || !req.body.battery.capacity ||
            !req.body.battery.voltage ){
            res.status(400).json({message:'Battery information incomplete!'});
        }

        const newBattery = new Battery(req.body.battery);

        newBattery.cuid = cuid();

        newBattery.save((err, saved) => {
            if (err) {
                res.status(500).send(err);
            }
            res.status(200).json({ battery: saved });
        });
    }
    catch (err) {
        console.log(err);
    }
}


TestDataController.createChargingStation = (req, res) => {
    try {
        if (!req.body.chargingStation.location ){
            res.status(400).json({message:'Battery information incomplete!'});
        }

        const newChargingStation = new ChargingStation(req.body.chargingStation);

        newChargingStation.cuid = cuid();

        newChargingStation.save((err, saved) => {
            if (err) {
                res.status(500).send(err);
            }
            res.status(200).json({ chargingStation: saved });
        });
    }
    catch (err) {
        console.log(err);
    }
}

// TestDataController.createUser = (req, res) => {
//     try {
//         if (!req.body.user.firstName || !req.body.user.lastName ||
//             !req.body.user.email || !req.body.user.password ){
//             res.status(400).json({message:'User information incomplete!'});
//         }
//
//         const newUser = new User(req.body.user);
//
//         newUser.cuid = cuid();
//
//         newUser.save((err, saved) => {
//             if (err) {
//                 res.status(500).send(err);
//             }
//             res.status(200).json({ user: saved });
//         });
//     }
//     catch (err) {
//         console.log(err);
//     }
// }

TestDataController.createUser = (req, res) => {
    try {
        if (!req.body.user.firstName || !req.body.user.lastName ||
            !req.body.user.email || !req.body.user.password ){
            res.status(400).json({message:'User information incomplete!'});
        }

        const newUser = new User(req.body.user);
        newUser.cuid = cuid();


        User.register(newUser, req.body.user.password, function(err, user) {
                if (err) {
                  res.status(500).json({ success: false, message: "Your account could not be saved. Error: ", err })
                } else {
                  res.status(200).json({success: true, message: "Your account has been saved"})
                }
              });

    }
    catch (err) {
        console.log(err);
    }
}

TestDataController.getAllOutBatteries = (req, res) => {
    try {
      res.status(200).json({ allOutBatteries: global.allBatteriesOut });
    }
    catch (err) {
        console.log(err);
    }
}

TestDataController.getUserById = (req, res) => {
    try{
        User.findById(req.params.id).exec((err, user) => {
            if (err) {
                res.status(500).send(err);
            }
            res.status(200).json({ user });
        });
    }
    catch(err){
        res.send(err);
    }
}

TestDataController.getUserByEmail = (req, res) => {
    try{
        User.findOne({email: req.params.email}).exec((err, user) => {
            if (err) {
                res.status(500).send(err);
            }
            res.status(200).json({ user });
        });
    }
    catch(err){
        res.send(err);
    }
}

export default TestDataController;
