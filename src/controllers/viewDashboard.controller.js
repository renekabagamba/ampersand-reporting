import SwapTransaction from '../models/swapTransaction.model';
import Driver from '../models/driver.model';
import ChargingStation from '../models/chargingStation.model';
import SwapStats from '../models/swapStats.model';

const ViewDashboardController =  {};

ViewDashboardController.getKeyStats = (req, res) => {
    try{

      SwapStats.aggregate([
              {
                "$lookup": {
                  "from": SwapTransaction.collection.name,
                  "localField": "swapTransactionId",
                  "foreignField": "_id",
                  "as": "swapTransaction"
                }
              },
               {
                "$group" : {
                            _id: { driver: "$swapTransaction.driver",
                                   date: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt"} }
                                 },
                            dailySwapCount: { $sum: 1 },
                            dailyEnergyUsed: { $sum: "$energyUsed" },
                            dailyDistanceCovered: { $sum: "$distance" }
                          }
                },
                {
                 "$group" : {
                             _id: "$_id.date",
                             avgDailySwaps: { $avg: "$dailySwapCount" },
                             avgDailyEnergyUsed: { $avg: "$dailyEnergyUsed" },
                             avgDailyDistanceCovered: { $avg: "$dailyDistanceCovered" }
                           }
                 },
                 { $sort : { _id : -1 } },

            ]).exec((err, keyStats) => {
                 if (err) {
                     res.status(500).send(err);
                 }
                 res.status(200).json({ keyStats });
             });
    }
    catch(err){
        res.send(err);
    }
};

ViewDashboardController.getDailySwapStats = (req, res) => {
    try{

      SwapStats.aggregate([
              {
                "$lookup": {
                  "from": SwapTransaction.collection.name,
                  "localField": "swapTransactionId",
                  "foreignField": "_id",
                  "as": "swapTransaction"
                }
              },
               {
                "$group" : {
                            _id: { driver: "$swapTransaction.driver",
                                   date: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt"} }
                                 },
                            dailySwapCount: { $sum: 1 },
                            dailyEnergyUsed: { $sum: "$energyUsed" },
                            dailyDistanceCovered: { $sum: "$distance" }
                          }
                },
                {
                  "$lookup": {
                    "from": Driver.collection.name,
                    "localField": "_id.driver",
                    "foreignField": "_id",
                    "as": "driver"
                  }
                },
                {
                  "$project": {
                    "_id": 1,
                    "dailySwapCount": 1,
                    "dailyEnergyUsed": 1,
                    "dailyDistanceCovered": 1,
                    "driver._id": 1,
                    "driver.firstName": 1,
                    "driver.lastName": 1,
                  }
                }

            ]).exec((err, dailySwapStats) => {
                 if (err) {
                     res.status(500).send(err);
                 }
                 res.status(200).json({ dailySwapStats });
             });
    }
    catch(err){
        res.send(err);
    }
};

ViewDashboardController.getAllSwapStats = (req, res) => {
    try{
        SwapStats.find()
                 .populate({ path: 'swapTransactionId', model: SwapTransaction,
                             populate: [{
                                          path: "driver",
                                          model: Driver,
                                          select: "firstName lastName"
                                        },
                                        {
                                          path: "chargingStation",
                                          model: ChargingStation,
                                          select: "location"
                                        }]
                          })
                 .sort('-createdAt')
                 .exec((err, swapStats) => {
                    if (err) {
                        res.status(500).send(err);
                    }
                    res.status(200).json({ swapStats });
                });
    }
    catch(err){
        res.send(err);
    }
};


export default ViewDashboardController;
