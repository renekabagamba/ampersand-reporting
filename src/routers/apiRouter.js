import express from 'express';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';

let router = express.Router();

// Controllers
import TestDataController from '../controllers/testData.controller';
import SwapBatteryController from '../controllers/swapBattery.controller';
import ViewDashboardController from '../controllers/viewDashboard.controller';

// APIs
// Test Data Related APIs
router.post('/vehicles', TestDataController.createVehicle);
router.post('/drivers', TestDataController.createDriver);
router.post('/batteries', TestDataController.createBattery);
router.post('/charging-stations', TestDataController.createChargingStation);
router.post('/users', TestDataController.createUser);
router.get('/users/:id', TestDataController.getUserById);
router.get('/users/email/:email', TestDataController.getUserByEmail);
router.get('/out-batteries', TestDataController.getAllOutBatteries);

// Swap Battery Related APIs
router.get('/charging-stations', SwapBatteryController.getAllChargingStations);
router.get('/drivers', SwapBatteryController.getAllDrivers);
router.get('/available-batteries', SwapBatteryController.getAllAvailableBatteries);
router.post('/swap-transactions', SwapBatteryController.recordSwapTransaction);
router.get('/swap-transactions/:driverId', SwapBatteryController.getLastSwap);

// Dashboard Related APIs
router.get('/swap-stats', ViewDashboardController.getAllSwapStats);
router.get('/key-stats', ViewDashboardController.getKeyStats);
router.get('/daily-swaps', ViewDashboardController.getDailySwapStats);

export default router;
