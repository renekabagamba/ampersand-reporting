import express from 'express';

let router = express.Router();

// UI routes
import index from '../routes/index';
import dashboard from '../routes/dashboard';
import swap from '../routes/swap';

// import login from '../routes/login';
// import profiles from '../routes/profiles';
// import profile from '../routes/profile';
// import register from '../routes/register';


//router.use('/', checkNotAuthenticated, index);
router.use('/dashboard', checkAuthenticated, dashboard);
router.use('/swap', checkAuthenticated, swap);
router.use('/', index);
//router.use('/dashboard', dashboard);
//router.use('/swap', swap);
// router.use('/login', login);
// router.use('/profiles', profiles);
// router.use('/profile', profile);
// router.use('/register/', register);


function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  }

  res.redirect('/')
}

function checkNotAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect('/dashboard');
  }
  next()
}


export default router;
