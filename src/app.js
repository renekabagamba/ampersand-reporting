import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import passport from 'passport';
import flash from 'express-flash';
import session from 'express-session';
import methodOverride from 'method-override';
import bodyParser from 'body-parser';

const app = express();

// Global variables
global.allBatteriesOut = new Set();
// Test data
global.allBatteriesOut.add("5f2d2a0ccd4bed0cd02c0c2b");
global.allBatteriesOut.add("5f2d28d47672c60ca70cc720");
global.allBatteriesOut.add("5f2d29327672c60ca70cc721");

import viewRouter from './routers/viewRouter';
import apiRouter from './routers/apiRouter';

require('./utils/db/index');

import initializePassport from './utils/passport/index';
initializePassport(passport);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../public')));

app.use(flash())
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(methodOverride('_method'))

app.use('/', viewRouter);
app.use('/api', apiRouter);

app.post('/login', passport.authenticate('local', {
  failureRedirect: '/',
  failureFlash: true }),
  function(req, res) {
    res.redirect('/dashboard');
  });

// // Log out
app.delete('/logout', (req, res) => {
  req.logOut()
  res.redirect('/')
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

export default app;
