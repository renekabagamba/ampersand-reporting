import mongoose, { Schema } from 'mongoose';

const DriverSchema = new Schema(
    {
        firstName:  { type: String, required: true },
        lastName:  { type: String, required: true },
        phoneNumber:  { type: String, required: true, unique : true,
                        dropDups: true },
        vehicle: { type: Schema.Types.ObjectId, ref: 'vehicle', unique : true,
                   dropDups: true }
    },
    {  timestamps: true , collection: 'drivers' }
);

export default mongoose.model('Driver', DriverSchema);
