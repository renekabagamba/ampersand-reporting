import mongoose, { Schema } from 'mongoose';

const SwapStatsSchema = new Schema(
    {
        swapTransactionId:  { type: Schema.Types.ObjectId, ref: 'swapTransaction',
                              required: true },
        energyUsed:  { type: Number },
        distance: {type: Number }

    },
    {  timestamps: true , collection: 'swap_stats' }
);

export default mongoose.model('SwapStats', SwapStatsSchema);
