import mongoose, { Schema } from 'mongoose';
// import bcrypt from 'mongoose-bcrypt';
import passportLocalMongoose from 'passport-local-mongoose';

const UserSchema = new Schema(
    {
      firstName: { type: String, required: true },
      lastName: { type: String, required: true },
      email: { type: String, required: true, unique: true },
      // password: { type: String, required: true, bcrypt: true },
      // password: { type: String, required: true },
      profilePic: { type: String }
    },
    {  timestamps: true , collection: 'users' }
);

// UserSchema.plugin(bcrypt);
UserSchema.plugin(passportLocalMongoose, {usernameField: 'email'});

export default mongoose.model('User', UserSchema);
