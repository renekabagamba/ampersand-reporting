import mongoose, { Schema } from 'mongoose';

const ChargingStationSchema = new Schema(
    {
        location: { type: String, unique : true, required : true, dropDups: true },
    },
    {  timestamps: true , collection: 'charging_stations' }
);

export default mongoose.model('ChargingStation', ChargingStationSchema);
