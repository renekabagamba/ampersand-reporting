import mongoose, { Schema } from 'mongoose';

const BatterySchema = new Schema(
    {
        serialNo: { type: String, unique : true, required : true, dropDups: true },
        capacity: { type: Number, required: true, default: 50 },
        voltage: { type: Number, required: true, default: 12 },
        description: { type: String }
    },
    {  timestamps: true , collection: 'batteries' }
);

export default mongoose.model('Battery', BatterySchema);
