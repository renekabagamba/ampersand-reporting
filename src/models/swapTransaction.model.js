import mongoose, { Schema } from 'mongoose';

const SwapTransactionSchema = new Schema(
    {
        batteryIn:  { type: Schema.Types.ObjectId, ref: 'battery' },
        batteryOut:  { type: Schema.Types.ObjectId, ref: 'battery',
                       required : true },
        fee:  { type: Number, required: true },
        chargingStation: { type: Schema.Types.ObjectId, ref: 'chargingStation',
                           required : true },
        driver: { type: Schema.Types.ObjectId, ref: 'driver', required : true },
        odometerReading: { type: String, required: true },
        batteryInCharge: { type: Number },
        batteryOutCharge: { type: Number, required: true }

    },
    {  timestamps: true , collection: 'swap_transactions' }
);

export default mongoose.model('SwapTransaction', SwapTransactionSchema);
