import mongoose, { Schema } from 'mongoose';
import bcrypt from 'mongoose-bcrypt';

const VehicleSchema = new Schema(
    {
        brand: { type: String, required: true },
        model: {type: String, required: true},
        plateNo: {type: String, unique : true, required : true, dropDups: true },
    },
    {  timestamps: true , collection: 'vehicles' }
);

VehicleSchema.plugin(bcrypt);

export default mongoose.model('Vehicle', VehicleSchema);
