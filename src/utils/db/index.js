
import mongoose from 'mongoose';
import config from '../../config';

mongoose.Promise = global.Promise;

console.log('Connecting to DB ...');

const connection = mongoose.connect(config.database.uri,
                                { useNewUrlParser: true});

connection
    .then(db => {
 //       logger.info(
  //          `Successfully connected to ${config.database.uri} MongoDB cluster.`
           // in ${ config.env } mode.`,
  //      );
        console.log(`Successfully connected to ${config.database.shortUri} MongoDB cluster.`)
        return db;
    })
    .catch(err => {
        if (err.message.code === 'ETIMEDOUT') {
 //           logger.info('Attempting to re-establish database connection.');
            console.log('Attempting to re-establish database connection.')
            mongoose.connect(config.database.uri);
        } else {
          console.log('Error while attempting to connect to database:');
          console.log(err);
   //         logger.error('Error while attempting to connect to database:');
   //         logger.error(err);
        }
    });

export default connection;
