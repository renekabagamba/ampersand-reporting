// import bcrypt from 'bcrypt';
import passportLocal from 'passport-local';
const LocalStrategy = passportLocal.Strategy;

import User from '../../models/user.model';

function initialize(passport) {

      passport.use(User.createStrategy());;
      passport.serializeUser(User.serializeUser());
      passport.deserializeUser(User.deserializeUser())
}


export default initialize;
